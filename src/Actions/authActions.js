
// authActions.js
export const login = (credentials) => {
    // Dispatch an action to log in the user
    return {
      type: 'LOGIN',
      payload: credentials,
    };
  };
  
  export const logout = () => {
    // Dispatch an action to log out the user
    return {
      type: 'LOGOUT',
    };
  };
